import React from 'react';

//import components
import HeaderNav from '../HeaderNav/index';
import SideMenu from '../SideMenu/index';
import MainContent from '../MainContent/index';
// import Breadcrumb from '../Breadcrumb/index';

function App() {
  return(
    <div className="app">
      <HeaderNav />
      <SideMenu/>
      <MainContent/>
    </div>
  )
}

export default App;
