import React from 'react';
import './SideMenu.css';
import Vector1 from '../../assets/img/Vector.jpg';
import Vector2 from '../../assets/img/Vector.png';
import Vector3 from '../../assets/img/Vector3.png';
import Vector4 from '../../assets/img/Vector4.png';

function SideMenu() {
    return(
            <div className="sidebar">
                <div className='wrapper-dash'>
                 <h1 className='dash'>Dashboard</h1>
                </div>
                <div className="sidebar-menu">                    
                    <a href='#top' id='kelas'>Kelas Saya</a>
                    <a href='#top' id='test'>Ambil Test</a>
                    <a href='#top' id='belajar'>Bahan Belajar</a>
                    <a href='#top' id='letter'>Commitment Letter</a>
                    <a href='#top' id='playbook'>Download Playbook</a>
                </div>
            </div>
    //    <div>
    //    <input type='checkbox' id='slide' name='' value=''></input>
    //    <div className='container'>
           
    //        <nav className='sidebar'>
    //            <label for='slide' className='toggle'><i className='material-icons'>dehaze</i></label>
    //            <ul>
    //                <li><h1>Dashboard</h1></li>
    //                <li><a href='#'>Kelas Saya</a></li>
    //                <li><a href='#'>Halo</a></li>
    //            </ul>
    //        </nav>
    //    </div>
    //    </div>

    )
}

export default SideMenu;