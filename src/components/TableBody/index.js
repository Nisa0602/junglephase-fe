import React from 'react';

function TableBody({ values = [] }) {
  return (
    <tbody>
      {values.map(rows => (
        <tr>
          {rows.map(column => (
            <td>{column}</td>
          ))}
        </tr>
      ))}
    </tbody>
  );
}

export default TableBody;
