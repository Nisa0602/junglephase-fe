import React from 'react';
import ConfusedImg from '../../assets/img/confused 1.png';

import Breadcrumb from '../Breadcrumb/index';
// import SideMenu from '../SideMenu/index'
import './MainContent.css';

function MainContent() {
    return(
    
        <div className="main">
           {/* <SideMenu/> */}
           <Breadcrumb/>
                <div className="card-content" id="content">
                    <div className='isi-konten'>
                        <img src={ConfusedImg} alt='confusedImage'></img>
                        <h1 className='info'>Kamu belum punya kelas nihh..</h1>
                        <button id='button-boot'>Gabung Bootcamp</button>
                    </div>
                </div>
        </div>

            
        // <div className="row">
        //         <div id='content' className='card-content'>
        //             <div className='img'>
        //                 <img src={ConfusedImg} alt='confusedImage'></img>
        //                 <h1 className='info'>Kamu belum punya kelas nihh..</h1>
        //                 <button id='button-boot' className='btn-large'>Gabung Bootcamp</button>
        //             </div>
        //         </div>
                
        // </div>
    )
}
export default MainContent;