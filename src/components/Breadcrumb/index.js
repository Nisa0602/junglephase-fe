import React from 'react';
import './Breadcrumb.css';
import SideMenu from '../SideMenu/index'

function BreadCrumb() {
    return(
        <div className='bread-crumb'>
            <a href='#dashboard'>dashboard / Kelas saya</a>
        </div>
    )
}

export default BreadCrumb;