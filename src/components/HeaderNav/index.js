import React from 'react';
// import Breadcrumb from '../Breadcrumb/index';
import SideMenu from '../SideMenu/index';
import './HeaderNav.css';
import logoBinar from '../../assets/img/logo binar.png'; 

function HeaderNav () {
    return(
      <nav>
        <div className='nav'>
          <img className="img1" src={logoBinar} alt='logoBinar'></img>
          <button id="button" className='btn-large right'>Keluar</button>
          <div id='nav-item' className="right">  
            <h1 id='name' className=''>Budi Handoko</h1>
            <p id='candidate' className=''>Candidate</p>
          </div>
        </div>
      </nav>

      // <nav>
      //   <div class="nav-wrapper">
      //     <img className="img1" src={logoBinar} alt='logoBinar'></img>
      //     <button id="button" className='btn-large right'>Keluar</button>
      //     <div id='nav-item' className="right">  
      //        <h1 id='name' className=''>Budi Handoko</h1>
      //        <p id='candidate' className=''>Candidate</p>
      //     </div>
      //   </div>
      // </nav>
    )
}

export default HeaderNav;